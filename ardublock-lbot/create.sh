#!/bin/bash
LIBRARIES=$(realpath "libraries")
SHARED=$(realpath "shared")

out() { cat > /dev/null; }

FORCE=0
for arg in "$@"; do
    case "$arg" in
        -f|--force)
            FORCE=1
            ;;
        -v|--verbose)
            out() { true; }
            ;;
    esac
done

echo "# Created containers:"
for link in link_*; do
    name=ardublock_$(echo $link | cut -d_ -f2-)
    if [ "$(docker ps -a --format '{{.Names}}' | grep "^$name$")" != '' ]
    then
        if [ $FORCE -eq 1 ]; then
            (docker stop "$name" || true) | out
            docker rm "$name" | out
        else
            echo "# $name already exists, ignoring (use -f to replace it)"
            continue
        fi
    fi
    ARDUBLOCK="$(readlink -f $link)"
    docker create -it\
        -e DISPLAY=$DISPLAY\
        -v $ARDUBLOCK:/home/arduino/Arduino/tools/ArduBlockTool/tool\
        -v $LIBRARIES:/home/arduino/Arduino/libraries\
        -v $SHARED:/home/arduino/shared\
        -v /tmp/.X11-unix/:/tmp/.X11-unix/\
        -v /dev/bus/usb:/dev/bus/usb\
        --privileged\
        --name "$name"\
        ardublock | out
    echo "docker start $name"
done
        #--device-cgroup-rule='c 166:* rmw'\
