#!/bin/sh

docker run -it --rm -e DISPLAY=$DISPLAY -v "$PWD/Arduino:/home/arduino/Arduino" -v /tmp/.X11-unix/:/tmp/.X11-unix/ -v /dev/bus/usb:/dev/bus/usb --privileged arduino "$@"
